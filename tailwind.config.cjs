/** @type {import('tailwindcss').Config} */
module.exports = {
	// mode: 'jit',
	content: [
		'./src/**/*.{html,js,svelte,ts}',
		'./node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}'
	],
	theme: {
		extend: {
			colors: {
				// primary: '#B32126',
				primary: {
					50: '#d99093',
					100: '#d17a7d',
					200: '#ca6467',
					300: '#c24d51',
					400: '#bb373c',
					500: '#b32126',
					600: '#a11e22',
					700: '#8f1a1e',
					800: '#7d171b',
					900: '#6b1417'
				},
				red: {
					50: '#d99093',
					100: '#d17a7d',
					200: '#ca6467',
					300: '#c24d51',
					400: '#bb373c',
					500: '#b32126',
					600: '#a11e22',
					700: '#8f1a1e',
					800: '#7d171b',
					900: '#6b1417'
				},
				background: '#eef0f3',
				'dark-blue': '#20435F'
			}
		}
	},
	plugins: [require('flowbite/plugin')]
};
