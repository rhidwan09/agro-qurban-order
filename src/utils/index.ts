export const formatRupiah = (number:number) => {
  return 'Rp ' + number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
};

export const generateRange = (number: number[]) => {
  const separator = ' - ';

  let result = '';

  for (let i = 0; i < number.length; i++) {
    result += formatRupiah(number[i]);

    if (i !== number.length - 1) {
      result += separator;
    }
  }

  return result;
}

export function slugify(str: string) {
  return str.toLowerCase().replace(/[^\w\s-]/g, '').replace(/[\s_]+/g, '-').replace(/^-+|-+$/g, '').replace(/-{2,}/g, '-');
}