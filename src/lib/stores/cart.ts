import { browser } from "$app/environment";
import type { ProductCart } from "$lib/models/types";
import { writable } from "svelte/store";

const productCart: ProductCart[] = []

export const cart = writable(productCart)

if (browser){
  // set initial value
  cart.set(JSON.parse(localStorage.getItem("cart")) || [])

  // update persisted when any update to cart
  cart.subscribe(value => {
    localStorage.setItem('cart', JSON.stringify(value));
  });
}

export function addToCart(product: ProductCart) {
  cart.update(items => {
    // Check if the product already exists in the cart
    const existingItem = items.find(item => item.product.id === product.product.id);
    
    if (existingItem) {
      // If the product already exists, increase its quantity by 1
      existingItem.quantity = existingItem.quantity + product.quantity;
    } else {
      // If the product does not exist, add it to the cart with quantity of 1
      items = [...items, { ...product, quantity: product.quantity }];
    }
    
    return items;
  });
}

export function removeFromCart(product: ProductCart) {
  cart.update(items => {
    // Filter out the product that matches the id of the item to be removed
    return items.filter(item => item.product.id !== product.product.id);
  });
}