import { writable } from "svelte/store"
import type { Member } from "$lib/models/types"
import { browser } from "$app/environment"

const member: Member[] = []

export const members = writable(member)

export const isModalFormMemberOpen = writable(false)

if (browser){
  // set initial value
  members.set(JSON.parse(localStorage.getItem("members")) || [])

  // update persisted when any update to members
  members.subscribe(value => {
    localStorage.setItem('members', JSON.stringify(value));
  });
}


export function addMember(member:Member){
  members.update(items => {
    return [...items, { ...member}];
  });
}

export function removeMember(member:Member){
  members.update(items => {
    return items.filter(item => item.name !== member.name);
  });
}