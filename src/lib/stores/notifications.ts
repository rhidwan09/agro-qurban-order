import { writable } from 'svelte/store'

type Notification = {
  title: string | 'Notification',
  type: 'error' | 'success',
  timer?: number | 2000
}

export const notifications = writable<Notification[]>([])

export function toast(notif: Notification) {
  notifications.update((state) => [
    {
      title: notif.title,
      type: notif.type
    }, ...state])
  setTimeout(removeToast, notif.timer ? notif.timer : 2000)
}

function removeToast() {
  notifications.update((state) => {
    return [...state.slice(0, state.length - 1)]
  })
}