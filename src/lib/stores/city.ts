import { writable } from "svelte/store"
import { browser } from "$app/environment"
import type { City } from "$lib/models/types"

const cityProps: City = {
  id: 0,
  name: ''
}

export const selectedCity = writable(cityProps)

if (browser){
  // set initial value
  selectedCity.set(JSON.parse(localStorage.getItem("selected_city")) || {
    id: 2,
    name: 'Bandung Raya'
  })

  // update persisted when any update to city
  selectedCity.subscribe(value => {
    localStorage.setItem('selected_city', JSON.stringify(value));
  });
}


export function setCity(city:City){
  selectedCity.update(item => city);
}