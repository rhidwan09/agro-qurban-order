export interface Member {
  id?: string;
  name: string;
  sex: 'm' | 'f';
  dob: string;
  fatherName?: string;
  motherName?: string;
  phone?: string;
}