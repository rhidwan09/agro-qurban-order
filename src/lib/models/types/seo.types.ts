export interface SEOInterface {
  title: string;
  description: string;
  image: string;
  url: string;
  siteName?: string;
}