import type * as z from 'zod'

export interface RadioButtonOption {
  label: string;
  value: string;
}

export interface RadioButtonProps {
  name: string;
  options: RadioButtonOption[];
  value?: string;
  required?: boolean;
  schema?: z.ZodString;
}