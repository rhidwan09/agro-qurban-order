import type { Member } from "./member.types";

export interface Product {
  id: string;
  slug?: string;
  image: string;
  title: string;
  price?: number;
  description?: string;
}
export interface ProductCart {
  product: Product;
  members: Member[];
  quantity: number
}

export interface ProductGroup extends Product {
  slugProductGroup?: string;
  link?: string;
  rangePrice?: number[]
}

export interface BannerProduct {
  id: string;
  title: string;
  link: string;
  imageSmall: string;
  imageLarge: string;
  labelCta: string;
}

export interface ProductValue {
  id: string;
  image?: string;
  title: string;
  description?: string;
}
export interface ProductFlow {
  id: string;
  image?: string;
  title: string;
  description?: string;
}

export type LandingPageProduct = {
  title: string,
  banners: BannerProduct[],
  products: ProductGroup[],
  productValues: ProductValue[] | [],
  productFlows: ProductFlow[] | []
}
// export type ProductTypes = Product | BannerProduct