import { error } from '@sveltejs/kit'
import type { PageLoad } from './$types'
import type {BannerProduct, ProductGroup, LandingPageProduct, ProductValue, ProductFlow} from '$lib/models/types'

export const load: PageLoad = async ({ params }) => {
  // const product = await getPostFromDatabase(params.slug);
 
  let banners: BannerProduct[]
  let products: ProductGroup[]
  let productValues: ProductValue[]
  let productFlows: ProductFlow[]

  let response: LandingPageProduct

  await new Promise((resolve) => setTimeout(() => resolve('ok'), 3000))

  if (params.slug === 'qurban-antar') {

    banners = [{
      id: '1',
      title: 'SALURKAN QURBANMU, SECARA TEPAT SASARAN',
      link: '/',
      imageSmall: '/bannerproductgroup.png',
      imageLarge: '/bannerproductgroup.png',
      labelCta: 'Cek Qurban Banten',
    },{
      id: '2',
      title: 'TEPAT SASARAN',
      link: '/',
      imageSmall: '/bannerproductgroup.png',
      imageLarge: '/bannerproductgroup.png',
      labelCta: 'Cek Qurban Banten',
    }]
    
    products = [
      {
        id: '1',
        image: '/kambing.png',
        title: 'KAMBING / DOMBA',
        rangePrice: [1700000, 3000000],
        description: '20Kg - 50Kg',
      },
      {
        id: '2',
        image: '/sapi.png',
        title: 'SAPI JAWA',
        rangePrice: [17000000, 35000000],
        description: '100Kg - 150Kg',
      },
      {
        id: '3',
        image: '/sapibali.png',
        title: 'SAPI BALI',
        rangePrice: [27000000, 55000000],
        description: '200Kg - 500Kg',
      },
    ]

    productValues = [
      {
        id: '1',
        title: 'Sesuai Syariah',
        image: '/kambing.png',
        description: 'Hewan sehat, cukup umur dan penyembelihan yang sesuai syariah'
      },
      {
        id: '2',
        title: 'Kebermanfaatan Luas',
        image: '/sapi.png',
        description: 'Dengan jangkauan yang tersebar menghasilkan kebermanfaatan yang semakin luas'
      },
      {
        id: '3',
        title: 'Harga Kompetitif',
        image: '/sapibali.png',
        description: 'Dengan pemberdayaan peternak lokal menghasilkan harga yang terbaik'
      }
    ]

    productFlows = [
      {
        id: '1',
        title: 'Pilih hewan qurban',
        image: '/kambing.png',
        description: ''
      },
      {
        id: '2',
        title: 'Pembayaran',
        image: '/sapi.png',
        description: ''
      },
      {
        id: '3',
        title: 'Penyembelihan',
        image: '/sapibali.png',
        description: ''
      },
      {
        id: '4',
        title: 'Laporan',
        image: '/sapibali.png',
        description: ''
      }
    ]

    response = {
      title: 'QURBAN ANTAR',
      banners,
      products,
      productValues,
      productFlows
    }

    return response;
  }

  if (params.slug === 'qurban-berbagi') {

    banners = [{
      id: '1',
      title: 'KEMAS QURBAN UNTUK KELUARGA RAWAN GIZI',
      link: '/',
      imageSmall: '/bannerproductgroup.png',
      imageLarge: '/bannerproductgroup.png',
      labelCta: 'GABUNG SEKARANG',
    }]
    
    products = [
      {
        id: '1',
        image: '/kambing.png',
        title: 'KAMBING / DOMBA',
        price: 1700000,
      },
      {
        id: '2',
        image: '/sapi.png',
        title: 'SAPI',
        price: 11900000,
      },
      {
        id: '3',
        image: '/sapibali.png',
        title: 'SAPI 1/7',
        price: 1700000,
      },
    ]

    productValues = [
      {
        id: '1',
        title: 'Sesuai Syariah',
        image: '/kambing.png',
        description: 'Hewan sehat, cukup umur dan penyembelihan yang sesuai syariah'
      },
      {
        id: '2',
        title: 'Kebermanfaatan Luas',
        image: '/sapi.png',
        description: 'Dengan jangkauan yang tersebar menghasilkan kebermanfaatan yang semakin luas'
      },
      {
        id: '3',
        title: 'Harga Kompetitif',
        image: '/sapibali.png',
        description: 'Dengan pemberdayaan peternak lokal menghasilkan harga yang terbaik'
      }
    ]

    productFlows = [
      {
        id: '1',
        title: 'Pilih hewan qurban',
        image: '/kambing.png',
        description: ''
      },
      {
        id: '2',
        title: 'Pembayaran',
        image: '/sapi.png',
        description: ''
      },
      {
        id: '3',
        title: 'Penyembelihan',
        image: '/sapibali.png',
        description: ''
      },
      {
        id: '4',
        title: 'Laporan',
        image: '/sapibali.png',
        description: ''
      }
    ]

    response = {
      title: 'QURBAN BERBAGI',
      banners,
      products,
      productValues,
      productFlows
    }

    return response;
  }

  if (params.slug === 'qurban-kaleng') {

    banners = [{
      id: '1',
      title: 'KEMAS QURBAN UNTUK KELUARGA RAWAN GIZI',
      link: '/',
      imageSmall: '/bannerproductgroup.png',
      imageLarge: '/bannerproductgroup.png',
      labelCta: 'GABUNG SEKARANG',
    }]
    
    products = [
      {
        id: '1',
        image: '/kambing.png',
        title: 'KALENG KORNET',
        price: 1700000,
      },
      {
        id: '2',
        image: '/sapi.png',
        title: 'KALENG RENDANG',
        price: 1700000,
      }
    ]

    productValues = [
      {
        id: '1',
        title: 'Sesuai Syariah',
        image: '/kambing.png',
        description: 'Hewan sehat, cukup umur dan penyembelihan yang sesuai syariah'
      },
      {
        id: '2',
        title: 'Kebermanfaatan Luas',
        image: '/sapi.png',
        description: 'Dengan jangkauan yang tersebar menghasilkan kebermanfaatan yang semakin luas'
      },
      {
        id: '3',
        title: 'Harga Kompetitif',
        image: '/sapibali.png',
        description: 'Dengan pemberdayaan peternak lokal menghasilkan harga yang terbaik'
      }
    ]

    productFlows = [
      {
        id: '1',
        title: 'Pilih hewan qurban',
        image: '/kambing.png',
        description: ''
      },
      {
        id: '2',
        title: 'Pembayaran',
        image: '/sapi.png',
        description: ''
      },
      {
        id: '3',
        title: 'Penyembelihan',
        image: '/sapibali.png',
        description: ''
      },
      {
        id: '4',
        title: 'Laporan',
        image: '/sapibali.png',
        description: ''
      }
    ]

    response = {
      title: 'QURBAN KALENG',
      banners,
      products,
      productValues,
      productFlows
    }
    
    return response;
  }

  if (params.slug === 'qurban-box') {

    banners = [{
      id: '1',
      title: 'SALURKAN QURBANMU, SECARA TEPAT SASARAN',
      link: '/',
      imageSmall: '/bannerproductgroup.png',
      imageLarge: '/bannerproductgroup.png',
      labelCta: 'Cek Qurban Banten',
    }]
    
    products = [
      {
        id: '1',
        image: '/kambing.png',
        title: 'Q-BOX RUMAH QURBAN',
        price: 1700000,
      },
    ]

    productValues = [
      {
        id: '1',
        title: 'Sesuai Syariah',
        image: '/kambing.png',
        description: 'Hewan sehat, cukup umur dan penyembelihan yang sesuai syariah'
      },
      {
        id: '2',
        title: 'Kebermanfaatan Luas',
        image: '/sapi.png',
        description: 'Dengan jangkauan yang tersebar menghasilkan kebermanfaatan yang semakin luas'
      },
      {
        id: '3',
        title: 'Harga Kompetitif',
        image: '/sapibali.png',
        description: 'Dengan pemberdayaan peternak lokal menghasilkan harga yang terbaik'
      }
    ]

    productFlows = [
      {
        id: '1',
        title: 'Pilih hewan qurban',
        image: '/kambing.png',
        description: ''
      },
      {
        id: '2',
        title: 'Pembayaran',
        image: '/sapi.png',
        description: ''
      },
      {
        id: '3',
        title: 'Penyembelihan',
        image: '/sapibali.png',
        description: ''
      },
      {
        id: '4',
        title: 'Laporan',
        image: '/sapibali.png',
        description: ''
      }
    ]

    response = {
      title: 'QURBAN BOX',
      banners,
      products,
      productValues,
      productFlows
    }
    
    return response;
  }
 
  throw error(404, 'Not found');
}