import type { PageServerLoad } from "./$types";
import { API_ODOO, API_ODOO_ACCESS_TOKEN } from '$env/static/private';
import type { BannerProduct, ProductGroup, ProductValue } from "$lib/models/types";

export const load: PageServerLoad = async ({ params }) => {
  const headers = {
    'access-token': API_ODOO_ACCESS_TOKEN
  }

  const [banner, productCategory, statistic, productValue] = await Promise.all([
    fetch(`${API_ODOO}/agrosurya_website.banner?filters=[('state', '=', 'active')]`, {headers}),
    fetch(`${API_ODOO}/agrosurya.product.category?filters=[('product_category_id', '=', 1), ('state', '=', 'active')]`, {headers}),
    fetch(`${API_ODOO}/agrosurya_website.notice?filters=[('name', '=','Statistik'), ('state', '=','active')]`, {headers}),
    fetch(`${API_ODOO}/agrosurya_website.notice?filters=[('name', '=', 'Value'), ('product_category_id', '=', 1), ('state','=','active')]`, {headers})
  ]);
  const [responseBanner, responseProductCategory, responseStatistic, responseProductValue] = await Promise.all([banner.json(), productCategory.json(), statistic.json(), productValue.json()]);
  
  const bannerData: BannerProduct[] = []
  const productCategoryData: ProductGroup[] = []
  const productValueData: ProductValue[] = []

  responseBanner.results.map(value => {
    bannerData.push({
      id:value.id,
      title: value.name,
      imageSmall: value.photo ? `data:image/jpg;base64,${value.photo}` : '',
      imageLarge: value.photo ? `data:image/jpg;base64,${value.photo}` : '',
      link: value.link ? value.link : '/',
      labelCta: 'Lihat sekarang'
    })
  })

  responseProductCategory.results.map(value => {
    productCategoryData.push({
      id: value.id,
      image: value.photo ? `data:image/jpg;base64,${value.photo}` : '/Aqiqah-Antar.jpg',
      title: value.name,
      link: value.link ? value.link : '/',
      description: value.description,
    })
  })

  responseProductValue.results.map(value => {
    productValueData.push({
      id: value.id,
      title: value.title,
      description: value.noted,
      image: value.icon ? `data:image/jpg;base64,${value.icon}` : '/Aqiqah-Antar.jpg',
    })
  })
  
  return {
    props: {
      banner: bannerData,
      productCategory: productCategoryData,
      statistics: responseStatistic.results,
      productValues: productValueData
    }
  };
}