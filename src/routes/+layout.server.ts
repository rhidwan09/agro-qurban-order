import type { LayoutServerLoad } from "./$types";
import { API_ODOO, API_ODOO_ACCESS_TOKEN } from '$env/static/private';
import type { BannerProduct, ProductGroup, ProductValue } from "$lib/models/types";

export const load: LayoutServerLoad = async ({ params }) => {
  const headers = {
    'access-token': API_ODOO_ACCESS_TOKEN
  }

  const [menu] = await Promise.all([
    fetch(`${API_ODOO}/agrosurya_website.menu?filters=[('menu_id','=',False),('state','=','active')]`, {headers}),
  ]);

  const menuData:any = []
  const [responseMenu] = await Promise.all([menu.json()]);

  responseMenu.results.map(value => {
    menuData.push({
      id: value.id,
      name: value.name,
      href: value.link ? value.link : '/'
    })
  })

  return {
    props: {
      menus: menuData
    }
  }
}