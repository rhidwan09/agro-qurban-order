import type { PageServerLoad } from "./$types";
import { API_ODOO, API_ODOO_ACCESS_TOKEN } from '$env/static/private';

export const load: PageServerLoad = async ({ params }) => {
  const headers = {
    'access-token': API_ODOO_ACCESS_TOKEN
  }

  const [cities] = await Promise.all([
    fetch(`${API_ODOO}/res.company?filters=[('agrosurya_entity','!=',False),('agrosurya_company_type','!=','pusat')]`, {headers}),
  ]);

  const cityData:any = []
  const [responseCities] = await Promise.all([cities.json()]);

  responseCities.results.map(value => {
    cityData.push({
      id: value.id,
      name: value.name,
    })
  })

  return {
    props: {
      cities: cityData
    }
  }
}