import {error} from '@sveltejs/kit'
import type { PageLoad, PageServerLoad } from "./$types";
import { API_ODOO, API_ODOO_ACCESS_TOKEN } from '$env/static/private';
import type { Product, ProductGroup } from "$lib/models/types";

export const load: PageServerLoad = async ({ params, url }) => {

  const {categoryProduct: paramsCategoryProduct} = params
  const queryCity = url.searchParams.get('city')

  const headers = {
    'access-token': API_ODOO_ACCESS_TOKEN
  }

  const [productCategory, product] = await Promise.all([
    fetch(`${API_ODOO}/agrosurya.product.category?filters=[('product_category_id', '=', 1), ('state', '=', 'active')]`, {headers}),
    fetch(`${API_ODOO}/agrosurya.product?filters=[('product_category_id.slug', '=', '${paramsCategoryProduct}'),('state', '=', 'active')]&field=['id','name','product_category_id.slug', 'slug', 'description','photo']`, {headers}),
  ]);

  
  const [responseProductCategory, responseProduct] = await Promise.all([productCategory.json(), product.json()]);

  const productCategoryData: ProductGroup[] = []
  const productData: Product[] = []
  
  responseProductCategory.results.map(value => {
    productCategoryData.push({
      id: value.id,
      image: value.photo ? `data:image/jpg;base64,${value.photo}` : '/Aqiqah-Antar.jpg',
      title: value.name,
      // link: value.link ? value.link : '',
      link: `/order/${value.slug}`,
      slugProductGroup: value.slug,
      description: value.description,
    })
  })

  const seo = productCategoryData.filter(item => item.slugProductGroup === paramsCategoryProduct)

  if(seo.length === 0){
    throw error(404, 'Page not found')
  }

  const SEOData = {
    title: seo[0].title,
    description: seo[0].description,
    image: seo[0].image,
    url: seo[0].link
  }

  responseProduct.results.map(value => {
    productData.push({
      id: value.id,
      image: value.photo ? `data:image/jpg;base64,${value.photo}` : '/banner-1.png',
      title: value.name,
      slug: value.slug,
      // link: value.link ? value.link : '',
      // link: `/order/${slugify(value.name)}`,dw
      description: value.description ? value.description : '',
    })
  })

  return {
    props: {
      seo: SEOData,
      productCategory: productCategoryData,
      products: productData
    }
  }
}